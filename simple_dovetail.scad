/*

Some sketches to work towards a nicer dovetail mechanism in OpenSCAD.

(c) Richard Bowman 2021, released under CERN-OHL-W v2

*/

use <libs/libdict.scad>;
use <libs/libofu.scad>;

function default_params() = dict_to_lookup_function([
    ["depth", 4],            // y distance between outer flat surface and tip
    ["angle", 60],           // angle of sloping part
    ["outer_flat", 6],       // width of outer flat parts
    ["overall_width", 30],   // width of whole structure
    ["overall_height", 16],  // height of whole structure
    ["block_depth", 12],     // y size of mounting block
    ["fillet_r", 0.5],       // fillet radius for rounded corners
    ["relief_r", 0.7],       // fillet radius for rounded corners
    ["lever", 6],            // distance from flat surface to pivot of clamp
    ["flex_l", 3],           // length of clamp flexure
    ["flex_t", 1.6],         // thickness of above
    ["clamp_t", 8],          // x dimension of clamping flange
    ["top_t", 2],            // thickness of the top solid layer
    ["bottom_t", 2],         // thickness of the bottom solid layer
    ["vertical_gap", 1],     // gap between clamp and top/bottom layers
    ["clamp_support_t", 0.5],// thickness of internal bridge support for clamp
    ["clamp_angle", 7],      // angle through which we allow the clamp to bend
]);

module block_sharp(p){
    // the block to which we attach the male dovetail 
    // or from whiuch we cut the female one

    w = p("overall_width");
    block = [w, p("block_depth")];

    translate([-w/2, -block[1]]) square(block);
}

module flange_r(p, width=ofu_tiny()){
    // the angled part of a male dovetail

    w = p("overall_width");
    flat = p("outer_flat");
    shiftx = [-width, 0];
    // we extend the parallelogram into the block slightly, 
    // at the same angle.
    shift_in = ofu_tiny()*[-cos(p("angle")), -sin(p("angle"))];

    polygon([
        female_point(p) + shift_in,
        male_point(p),
        male_point(p) + shiftx,
        female_point(p) + shiftx + shift_in
    ]);
}

function male_point(p) = let(
    w = p("overall_width"),
    flat = p("outer_flat"),
    depth = p("depth"),
    angle = p("angle")
) [w/2 - flat + depth/tan(angle), depth];

function female_point(p) = let(
    w = p("overall_width"),
    flat = p("outer_flat"),
    depth = p("depth"),
    angle = p("angle")
) [w/2 - flat, 0];

module dovetail_section_m_sharp(p){
    // A male dovetail, before any filleting of the corners
    difference(){
        union(){
            block_sharp(p);

            hull() ofu_reflect([1, 0]) flange_r(p);
        }

        // relieve internal corners
        ofu_reflect([1, 0]) translate(female_point(p)){
            circle(p("relief_r"));
        }        
    }
}

module dovetail_section_f_sharp(p){
    // A female dovetail, before any filleting of the corners
    difference(){
        union(){
            block_sharp(p);
        }
        
        hull() ofu_reflect([1, 0]) mirror([0,1]) flange_r(p);

        // relieve internal corners
        hull() ofu_reflect([1, 0]) translate(-male_point(p)){
            circle(p("relief_r"));
        }        
    }
}

module rotate_repeat(angle){
    union(){
        children();
        rotate(angle) children();
    }
}

module clamp_frame(p){
    // place the origin at the pivot point of the clamp
    // and align y axis with the dovetail angle
    translate(female_point(p)) rotate(p("angle") - 90){
        translate([0, -p("lever")]) children();
    }
}

module clamp_cutout_2d(p){
    // 2D cutout to make a male dovetail clamp
    fillet_r = p("fillet_r");
    fp = female_point(p);
    mp = male_point(p);
    lever = p("lever");
    flex_l = p("flex_l");
    flex_t = p("flex_t");
    clamp_t = p("clamp_t");
    relief_r = p("relief_r");
    clamp_angle = p("clamp_angle");
    $fn=16;
    clamp_frame(p){
        // between nut and screw
        hull(){
            translate([0, fillet_r + flex_t/2]) circle(fillet_r);
            translate([0, lever]) circle(fillet_r); 
        } 
        // next to flexure
        hull() ofu_reflect([1,0]){
            translate([flex_l/2, fillet_r + flex_t/2]) circle(fillet_r);
        }
        // behind clamp
        ofu_sequential_hull(){
            translate([flex_l/2, -fillet_r - flex_t/2]) circle(fillet_r);
            rotate_repeat(clamp_angle) translate([-clamp_t, -fillet_r - flex_t/2]){
                circle(fillet_r);
            }
            rotate_repeat(clamp_angle) translate([-clamp_t, 99]) circle(fillet_r);
        } 
    }
}
module clamp_cutout_empty_2d(p){
    // 2D cutout to make a male dovetail clamp
    union(){
        clamp_cutout_2d(p);

        // take the hull of just the internal part
        hull() intersection(){
            clamp_cutout_2d(p);
            hull() ofu_repeat([-99, 0], 2, center=false){
                clamp_frame(p) ofu_reflect([0, 1]) {
                    translate([0, p("lever")]) circle(p("relief_r"));
                }
            }
        }
    }
}
module clamp_cutout_base_2d(p){
    // 2D cutout to separate the point of the clamp from the base
    fillet_r = p("fillet_r");
    relief_r = p("relief_r");
    lever = p("lever");
    
    union(){
        // separate the flange from the block
        hull() translate(female_point(p)){
            circle(relief_r);
            translate([-p("clamp_t") - relief_r, 0]) circle(relief_r);
        }

        // take the hull of just the external part
        intersection(){
            clamp_cutout_2d(p);
            hull() ofu_reflect([1, 0]) translate(female_point(p)){
                circle(p("relief_r"));
                translate([0, 99]) circle(p("relief_r"));
            }
        }
    }
}
module clamp_back_2d(p, extra_l=0, extra_r=0){
    // back of the internal part of the clamp
    length = p("clamp_t") + p("fillet_r") + extra_l + extra_r;
    clamp_frame(p){
        translate([-length + extra_r, -p("flex_t")/2]){
            square([length, p("flex_t")]);
        }
    }
}
module clamp_support_2d(p){
    // a bridge to support the internal part of the clamp
    // this sits underneath the back of the clamp
    clamp_back_2d(
        p, 
        extra_l=3*p("fillet_r"), 
        extra_r=p("flex_l")/2 + ofu_tiny()
    );
}
module clamping_flange_2d(p){
    convex_fillet(p) difference(){
        union(){
            hull(){
                // external end
                flange_r(
                    p, 
                    width=(
                        (p("clamp_t") - p("fillet_r"))
                        /sin(p("angle"))
                    )
                );
                // internal end
                clamp_back_2d(p);
            }

            // add the flexure to join to the block. 
            clamp_back_2d(
                p, 
                extra_r=p("flex_l") + p("fillet_r"),
                extra_l=-p("fillet_r")
            );
        }

        clamp_cutout_2d(p);
    }
}

module clamping_flange(p){
    // The moving part that makes the right hand flange
    // clamp the female dovetail
    gap = p("vertical_gap");
    bottom = p("bottom_t") + gap + p("clamp_support_t");
    top = p("overall_height") - gap - p("top_t");
    translate([0,0,bottom]) linear_extrude(top - bottom){
        clamping_flange_2d(p);
    }
}

module clamping_bolt_and_nut(p){
    // The counterbored screw and nut that clamp the dovetail
    h = p("overall_height");
    clamp_frame(p) translate([0, p("lever") - 2, h/2]){
        $fn = 16;
        // Counterbored hole for screw
        rotate([0, 90, 0]){
            cylinder(d=3*1.2, h=99);
            translate([0,0,p("fillet_r") + 4]) cylinder(d=3*1.3*2, h=99);
        }
        // Nut trap, with angled entry
        rotate([0, -90, 0]){
            cylinder(d=3*1.2, h=8);
            translate([0,0,p("fillet_r") + 1.5]) rotate([0,0,60]) ofu_sequential_hull(){
                // TODO: replace this with a proper parametric nut trap!
                cylinder(r=3*1.1, h=3.2, $fn=6);
                //translate([2,0,0]) cylinder(r=3*1.2, h=2.8, $fn=6);
                translate([99,0,0]) cylinder(r=3*1.1, h=3.2, $fn=6);
            }
        }
    }
}

module clamp_support(p){
    // a bridge to support the internal part of the clamp
    gap = p("vertical_gap");
    bottom = p("bottom_t") + gap;
    translate([0,0,bottom]) linear_extrude(p("clamp_support_t")){
        clamp_support_2d(p);
    }
}

module convex_fillet(p){
    // smooth the convex corners
    $fn=12;

    offset(p("fillet_r")) offset(-p("fillet_r")){
        children();
    }
}
module concave_fillet(p){
    // smooth the concave corners
    $fn=12;

    offset(-p("fillet_r")) offset(p("fillet_r")){
        children();
    }
}

module dovetail_section_m(p, relief=true){
    convex_fillet(p){
        dovetail_section_m_sharp(p, relief=relief);
    }
}

module undercut_male_dovetail(p){
    // Chamfer the bottom of the mating faces to avoid
    // wonkiness due to "elephant's foot" issues
    minkowski(){
        mirror([0,1,0]) linear_extrude(ofu_tiny()){
            dovetail_section_f_sharp(p);
        }

        cylinder(r1=2, r2=ofu_tiny(), h=2, $fn=16, center=true);
    }
}
module dovetail_clamp_m(p){
    // male dovetail with clamping arm
    h = p("overall_height");
    difference(){
        union(){
            difference(){
                linear_extrude(h) convex_fillet(p) difference(){
                    dovetail_section_m_sharp(p);

                    clamp_cutout_base_2d(p);
                }

                // void for clamp
                translate([0,0,2]) linear_extrude(h-4){
                    concave_fillet(p) clamp_cutout_empty_2d(p);
                }
            }

            // clamping flange
            clamping_flange(p);
            clamp_support(p);
        }

        clamping_bolt_and_nut(p);
        
        // work around "elephant's foot"/brim on mating faces
        undercut_male_dovetail(p);
    }
}



module dovetail_f(p, height=50){
    h = height;
    linear_extrude(h) convex_fillet(p){
        dovetail_section_f_sharp(p);
    }
}

%mirror([0,1,0]) dovetail_f(default_params());
//translate([0,25,0]) dovetail_f(default_params());
render(6) dovetail_clamp_m(default_params());
