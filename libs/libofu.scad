/*

OpenFlexure Utilities

The functions defined here are used in lots of places around the 
OpenFlexure project.

Released under CERN-OHL-W-V2

*/

use <./libofu_private.scad>;

function ofu_tiny() = tiny();

// Set the third element of a 3D size to zero.  
// A 2D size will have a zero appended, with no error
function ofu_zeroz(size) = zeroz(size);

module ofu_reflect(axis){ 
    //reflects its children about the origin, but keeps the originals
	reflect(axis) children();
}
module ofu_repeat(delta,N,center=false){ 
    //repeat something along a regular array
    repeat(delta,N,center) children();
}

module ofu_nut(d,h=-1,center=false,fudge=1.18,shaft=false){ //make a nut, for metric bolt of nominal diameter d
    // A hexagonal cut-out for a nut
	//d: nominal bolt diameter (e.g. 3 for M3)
	//h: height of nut
	//center: works as for cylinder
	//fudge: multiply the diameter by this number (1.22 works when vertical)
	//shaft: include a long cylinder representing the bolt shaft, diameter=d*1.05
    nut(d,h,center,fudge,shaft);
}

module ofu_nut_from_bottom(d,h=-1,fudge=1.2,shaft=true,chamfer_r=0.75,chamfer_h=0.75){ //make a nut, for metric bolt of nominal diameter d
	// A nut trap where the nut is inserted from the -z direction
    //d: nominal bolt diameter (e.g. 3 for M3)
	//h: height of nut
	//center: works as for cylinder
	//fudge: multiply the diameter by this number (1.22 works when vertical)
	//shaft: include a long cylinder representing the bolt shaft, diameter=d*1.05
	nut_from_bottom(d=d,h=h,fudge=fudge,shaft,chamfer_r=shaft_chamfer_r,chamfer_h=shaft_chamfer_h);
}
//nut_from_bottom(4,chamfer_h=4,h=7);

module ofu_nut_y(d,h=-1,center=false,fudge=1.15,extra_height=0.7,shaft=false,shaft_length=0,top_access=false){ //make a nut, for metric bolt of nominal diameter d
	// A nut trap where the shaft of the screw would be the y axis
    //d: nominal bolt diameter (e.g. 3 for M3)
	//h: height of nut
	//center: works as for cylinder
	//fudge: multiply the diameter by this number (1.22 works when vertical)
	//shaft: include a long cylinder representing the bolt shaft, diameter=d*1.05
    //top_access: extend the nut upwards to allow it to drop in.
	nut_y(d,h,center,fudge,extra_height,shaft,shaft_length,top_access);
}

module ofu_screw_y(d,h=-1,center=false,fudge=1.05,extra_height=0.7,shaft=false,shaft_length=999){ //make a nut, for metric bolt of nominal diameter d
	//d: nominal bolt diameter (e.g. 3 for M3)
	//h: height of screw head (set to 999 for counterbore)
	//center: works as for cylinder
	//fudge: multiply the diameter by this number (1.22 works when vertical)
	//shaft: include a long cylinder representing the bolt shaft, diameter=d*1.05
	 screw_y(d,h=h,center=center,fudge=fudge,extra_height=extra_height,shaft=shaft,shaft_length=shaft_length);
}

module ofu_cyl_slot(r=1, h=1, dy=2, center=false){
    // An elongated cylinder use to make a slot for a screw. Slot is oriented in the y direction
    // r: raduis of the slots
    // h: the height
    // dy: the length of the slot (centre to centre on circles) total length is dy+2*r
    // center: if true the shape is centred on all axes.
    cyl_slot(r, h, dy, center);
}


module ofu_unrotate(rotation){
	//undo a previous rotation, NB this is NOT the same as rotate(-rotation) due to ordering.
	unrotate(rotation);
}

module ofu_smatrix(xx=1,yy=1,zz=1,xy=0,xz=0,yx=0,yz=0,zx=0,zy=0, xt=0, yt=0, zt=0){
    //apply a matrix transformation, specifying the matrix sparsely
    //this is useful because most helpful matrices are close to the identity.
    smatrix(xx,yy,zz,xy,xz,yx,yz,zx,zy, xt, yt, zt) children();
}

module ofu_sequential_hull(){
	//given a sequence of >2 children, take the convex hull between each pair - a helpful, general extrusion technique.
    //NB there is a duplicate of this function called sequential_hull.  It should be identical.  This
    //is obviously not ideal, but due to the handling of children() we can't just wrap it.  The duplicate is in 
    //libofu_private.scad.
	for(i=[0:$children-2]){
		hull(){
			children(i);
			children(i+1);
		}
	}
}

module ofu_hole_from_bottom(r, h, base_w=-1, dz=0.5, big_bottom=true){
    // This creates a cut-out that can be used to make a hole in a large
    // bridge, without too much spaghetti!
    hole_from_bottom(r=r, h=h, base_w=base_w, dz=dz, big_bottom=big_bottom);
}


module ofu_trylinder(r=1, flat=1, h=tiny(), center=false){
    //Halfway between a cylinder and a triangle.
    //NB the largest cylinder that fits inside it has r=r+f/(2*sqrt(3))
    //One of the sides is parallel with the X axis
    trylinder(r=r, flat=flat, h=h, center=center);
}

module ofu_trylinder_selftap(nominal_d=3, h=10, center=false){
    // Make a trylinder that you can self-tap a machine screw into.
    // The size is deliberately a bit big for small holes, so that
    // it compensates for splodgy printing
    trylinder_selftap(nominal_d=nominal_d, h=h, center=center);
}

